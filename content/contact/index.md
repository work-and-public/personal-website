+++
title = "Contact Me"
description = "Section with my contacts."
disableToc = true
+++

## [LinkedIn](https://www.linkedin.com/in/paulorafaelmoreira/)

I am available at [LinkedIn](https://www.linkedin.com/in/paulorafaelmoreira/).

## [Email](mailto:rafael@homelabrafael.com)

Feel free to contact me via email me at [rafael@homelabrafael.com](mailto:rafael@homelabrafael.com).

## [Resume](https://gitlab.com/work-and-public/resume/-/blob/master/generated/resume.pdf)

Here is my _more detailed_ resume, with more information about my work history and academic history, alongside a very small exposure of the technologies I am proficient at.

This Resume is available at my [GitLab Repository](https://gitlab.com/work-and-public/resume).

![Resume](./resume.svg)
