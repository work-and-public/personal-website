+++
title = "About Me"
description = "Section with information about me."
disableToc = true
+++

## Who am I?

I am an young DevOps Engineer eager to learn new tecnhologies and patterns in this big world of Cloud Computing and constant technological advancements.

I am from Portugal and I have a Bachelor's degree in Informatics Engineering from ISEP. I have a background (high-school) in Sciences and Technologies.

I am currently taking a Master's Degree in Software Engineering, also at ISEP.

## What do I do?

I work with multiple technology stacks to deliver development changes to multiple environments (CI/CD), as smooth as changes can be.

I also design and maintain infrastructure in cloud vendors such as AWS and Azure. I have built custom infrastructures, using services such as EC2, ElasticLoadBalancers, RDS, and I have build Kubernetes infrastructures, with self-managed FOSS technologies and AWS Managed services (EKS).

I work on tools to ease developer's problems during their work, to prevent catastrophes and minimize the impact of threats.

## What technologies do I work with?

I work with Kubernetes (K8s), Docker and other containerization technologies (Podman, Containerd).

I am proficient in BASH, Typescript (mainly used in Infrastructure as Code or _Kubernetes as Code_ projects) and I can manage Linux-based systems (Ubuntu, Alma Linux, CentOS, Amazon Linux, amongst others).

## What am I currently learning?

I am currently improving my Python skills, to begin replacing some of my BASH scripts with Python (this topic deserves a post itself) and I am also learning Go, due to the fact that during my experiments with K8s I have had the need to develop custom tools, such as controllers, and Go feels like the _go-to_ language to use for this purpose.

I plan on learning Terraform and improve my Ansible skills later, to go full cloud-agnostic (in conjunction with K8s).

## Where do I store my code?

I use [GitLab](https://gitlab.com/rafaelmoreira1180778) to host my code for all my projects, alongside multiple thoughts and small scripts that I develop daily.

I also have a [GitHub](https://github.com/RafaelMoreira1180778) but I don't host any code there. Coming from a daily user of GitLab, I prefer the CI/CD mechanisms of it compared to GitHub.

## How may I contact you?

Please refer to the ["Contact Me" webpage](../contact/) of my blog for more information.
